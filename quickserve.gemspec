Gem::Specification.new do |s|
  s.name = "quickserve"
  s.version = "0.0.2"
  s.has_rdoc = false
  s.platform = Gem::Platform::RUBY
  s.author = "Yuri Tomanek"
  s.email = %q{yuri@tomanek.com.au}
  s.summary = %q{A quick static server}
  s.homepage = %q{http://github.com/yuritomanek/quickserve}
  s.description = %q{A quick way to serve static files}
  s.files = ["bin/quickserve"]
  s.executables = ["quickserve"]
  s.add_dependency 'webrick', '1.3.1'
end